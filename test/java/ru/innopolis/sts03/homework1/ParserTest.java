package ru.innopolis.sts03.homework1;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;


/**
 * Created by mmm on 16.12.2016.
 */
public class ParserTest {

    @Test
    public void parse() throws Exception {
        Parser p = new Parser("яяя", new ValidatorImpl());
        List<String> resList = p.parse();   //тест падает

        String[] pArr = new String[resList.size()];
        pArr = resList.toArray(pArr);

        String[] etalon = {"яяя"};
        //assertArrayEquals(pArr, etalon);
        //Assert.assertArrayEquals(new String[]{"яяя"}, etalon);

        if ( Arrays.equals(pArr, etalon) ) {
            System.out.println("true");
        }

//        assertTrue("", Arrays.equals(pArr, etalon));
//        System.out.println(Arrays.equals(pArr, etalon) == false);
//        assert Arrays.equals(pArr, etalon);
        assertTrue("",Arrays.equals(pArr, etalon) == true);
//        assertArrayEquals(pArr, etalon);

    }

}
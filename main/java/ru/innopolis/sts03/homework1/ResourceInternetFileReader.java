package ru.innopolis.sts03.homework1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс - считыватель файла-ресурса
 */
public class ResourceInternetFileReader extends ResourceReader {
    private static Logger logger = LoggerFactory.getLogger(ResourceInternetFileReader.class);

    private BufferedReader reader;

    /**
     * Конструктор принимает в качестве параметра имя файла
     * */
    public ResourceInternetFileReader(Object resource) {
        super(resource);
        URLConnection connection = null;
        try {
            connection = new URL((String) this.resource).openConnection();
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream())
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<String> readData() {
        //результат
        List<String> words = null;

        String line = null;
        try {
            line = reader.readLine();

            //Парсим данные - получаем набор русских слов
            words = this.parseData(line);
            if ( words == null ) {
                //ошибка чтения - неправильные данные в файле
                logger.error("Ошибка чтения - неправильные данные в файле");
                throw new RuntimeException("Ошибка - данные в файле не верны");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return words;
    }

    @Override
    public boolean ready() {
        return false;
    }

}

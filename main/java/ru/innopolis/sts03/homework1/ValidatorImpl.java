package ru.innopolis.sts03.homework1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Класс - валидатор
 */
public class ValidatorImpl implements StrValidator {

    /**
     * Метод проверяет - есть ли в строке латинские символы
     * @param st - входная строка для проверки
     * @return true - в случае если нет латинских символов, false - в противном случае
     * */
    @Override
    public boolean validate(String st) {
        Pattern patternEng = Pattern.compile("[A-Za-z]+", Pattern.CASE_INSENSITIVE);
        Matcher mEng = patternEng.matcher(st);

        //Если найден хоть один символ латиницы, то false
        if ( mEng.find() ) {
            //неверные символы в строке - есть латиница
            return false;
        } else {
            return true;
        }
    }
}

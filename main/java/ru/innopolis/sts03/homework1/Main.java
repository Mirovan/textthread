package ru.innopolis.sts03.homework1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Главый поток.
 * На вход подаются ключи и ресурсы,в зависимости от ключа
 */
public class Main {
    private static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        args = new String[]{
//                "-f", "D:\\java_projects\\idea\\home1\\src\\main\\resources\\input1.txt",
//                "-f", "D:\\java_projects\\idea\\home1\\src\\main\\resources\\input2.txt",
//                "-f", "D:\\java_projects\\idea\\home1\\src\\main\\resources\\input3.txt",
//                "-f", "D:\\java_projects\\idea\\home1\\src\\main\\resources\\book.txt",
                "-f", "D:\\java_projects\\idea\\home1\\src\\main\\resources\\input_hello.txt",
                "-f", "D:\\java_projects\\idea\\home1\\src\\main\\resources\\input_buy.txt",
//                "-url", "http://it.innopolis.ru/",
//                "-s", "system"
        };


        List<Thread> threadsList = new ArrayList<>();

        //анализируем ключи запуска программы
        for (int i=0; i<args.length; i++) {
            String type = args[i];
            i++;
            String path = args[i];

            //создаем поток для ресурса
            ResourceThreadRunner rtr = new ResourceThreadRunner(type, path);
            Thread t = new Thread(rtr);
            threadsList.add( t );
        }

        //Создаём свой менеджер потоков,в котором запускаем все потоки
        ThreadManager tmanager = new ThreadManager(threadsList);
        Thread tm = new Thread(tmanager);
        tm.start();

    }
}

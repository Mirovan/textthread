package ru.innopolis.sts03.homework1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Алгоритм:
 * 1) Запустили поток - анализатор текста
 * 2) Каждый момент времени изменяется результат, т.к. приходят новые данные из других потоков (где читаются ресурсы)
 * 3) Каждый момент времени, выводим результат - <слово, число вхождений>
 */
public class TextAnalizator {
    private static Logger logger = LoggerFactory.getLogger(TextAnalizator.class);

    private static Map<String, Integer> words = new HashMap<>();

    /**
     * Метод добавления данных в хеш-таблицу
     */
    public static void addData(String[] data) {
        logger.info("Add data to map: " + data);

        synchronized (words) {
            for (int i = 0; i < data.length; i++) {
                int count = 1;
                if (!words.containsKey(data[i])) {  //ToDo: Несинхр
                    words.put(data[i], count);    //вставляем новый элемент
                } else {
                    count = words.get(data[i]) + 1;
                    words.replace(data[i], count);     //Обновляем данные
                }
                //Выводим изменения
                System.out.println(data[i]  + " " + count);
            }
        }

    }

    /**
     * Метод возвращает хеш-таблицу со списком слов
     * */
    public static Map<String, Integer> getWords() {
        return words;
    }

    /**
     * Возвращает значение по ключу
     * @param element - ключ
     * @return int k - значение по ключу
     * */
    public static int getElementCount(String element) {
        synchronized (words) {
            return words.get(element);
        }
    }
}

package ru.innopolis.sts03.homework1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by mmm on 15.12.2016.
 */
public class ThreadManager implements Runnable {
    private static Logger logger = LoggerFactory.getLogger(ThreadManager.class);

    //список потоков
    List<Thread> threadsList;

    public ThreadManager(List<Thread> threadsList) {
        this.threadsList = threadsList;
    }

    //Есть ли ошибка в потоках, если в каком либо потоке возникла ошибка - перменная принимает значение true
    static volatile boolean hasException = false;

    @Override
    public void run() {
        //запускаем все потоки
        ExecutorService service = Executors.newCachedThreadPool();
        for(Thread t: threadsList) {
            service.submit(t);
        }
        service.shutdown();

        try {
            //ждем завершение всех потоков
            while (!service.awaitTermination(1, TimeUnit.SECONDS)) {
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if ( !hasException ) {
            System.out.println("FINISH !!!!!!!!!");
            System.out.println("All words count: " + TextAnalizator.getWords().size());
        } else {
            System.out.println("Error: Ошибка во врем выполнения. Недопустимые данные в файле");
        }

    }


    /**
     *
     * @return true - если хотя бы один поток работает
     * */
    private boolean listThreadsIsAlive() {
        for (Thread t: threadsList) {
            if (t.isAlive()) return true;
        }
        return false;
    }
}

package ru.innopolis.sts03.homework1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.Reader;
import java.util.List;

/**
 * Класс - поток для работы с ресурсами
 */
public class ResourceThreadRunner implements Runnable {
    private static Logger logger = LoggerFactory.getLogger(ResourceThreadRunner.class);

    private ResourceReader resReader;   //читатель ресурса

    /**
     * Конструктор
     * */
    public ResourceThreadRunner(String resourceType, String resourcePath) {
        if (resourceType.equals("-f")) {
            this.resReader = new ResourceFileReader(resourcePath);
        } else if (resourceType.equals("-url")) {
            this.resReader = new ResourceInternetFileReader(resourcePath);
        } else {
            //TODO Нет такого ресурса, что-то не так
            logger.error("Error: ресурс не найден");
        }
    }


    @Override
    public void run() {
        logger.info("Run thread. Resource: " + this.resReader.toString());
        //читаем данные из ресурса

        String line;
        while ( resReader.ready() && !ThreadManager.hasException ) {
            try {
                List<String> data = resReader.readData();
                String[] dataArr = new String[data.size()];
                dataArr = data.toArray(dataArr);
                TextAnalizator.addData(dataArr);
                for (int i = 0; i < dataArr.length; i++) {
                     int count = TextAnalizator.getElementCount(dataArr[i]);
                    //Выводим изменения
                    //if (!ThreadManager.hasException) System.out.println(dataArr[i]  + " " + count);
                }

            } catch(Exception e) {
                //завершить все потоки
                ThreadManager.hasException = true;
                //System.out.println("ОШииииииииииииибкааааааааааааа RUNNER");
            }
        }
    }

}
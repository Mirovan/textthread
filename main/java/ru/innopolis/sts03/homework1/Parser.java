package ru.innopolis.sts03.homework1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by mmm on 15.12.2016.
 */
public class Parser {
    private static Logger logger = LoggerFactory.getLogger(Parser.class);

    //валидатор
    private StrValidator validator;

    //Входная строка
    private String data;

    /**
     * КОнструктор
     * */
    public Parser(String data, StrValidator validator) {
        this.data = data;
        this.validator = validator;
    }

    /**
     * Метод возвращает список кириллическиз слов или null если присутствуют недопустимые символы
     *
     * @return resList - список слов
     * */
    public List<String> parse() {
        logger.info("Parse data: " + data);

        //если строка не прошла валидацию, возвращаем null
        if ( !this.validator.validate(data) ) {
            return null;
        }

        String[] lineData = data.split(" ");    //читаем построчно и парсим в массив через пробел

        List<String> resList = new ArrayList<>();   //результат

        Pattern patternRus = Pattern.compile("[А-Яа-я]+", Pattern.CASE_INSENSITIVE);
        //находим нужные кириллические слова
        for (int i = 0; i < lineData.length; i++) {
            Matcher mRus = patternRus.matcher(lineData[i]);
            //перебираем все вхождения русских слов без учета других символов
            while (mRus.find()) {
                resList.add(mRus.group());   //слово соответствует русскому алфавиту, отправляем в результирующий массив
            }
        }
        return resList;
    }

}

package ru.innopolis.sts03.homework1;


/**
 * Проверка на валидность строки
 */
public interface StrValidator {

    /**
     * Метод проверяет - есть ли в строке латинские символы
     * @param st - входная строка для проверки
     * @return true - в случае если нет латинских символов, false - в противном случае
     * */
    public boolean validate(String st);

}

package ru.innopolis.sts03.homework1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс - считыватель файла-ресурса
 */
public class ResourceFileReader extends ResourceReader {

    private static Logger logger = LoggerFactory.getLogger(ResourceFileReader.class);

    private BufferedReader reader;

    /**
     * Конструктор принимает в качестве параметра - URL адрес
     * */
    public ResourceFileReader(Object resource) {
        super(resource);
        String filename = (String) this.resource;
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(filename);
            reader = new BufferedReader(fileReader);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    @Override
    public List<String> readData() {
        //результат
        List<String> words = null;
/*
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
*/
        String line = null;
        try {
            line = reader.readLine();

            //Парсим данные - получаем набор русских слов
            words = this.parseData(line);
            if ( words == null ) {
                //ошибка чтения - неправильные данные в файле
                logger.error("Ошибка чтения - неправильные данные в файле");
                throw new RuntimeException("Ошибка - данные в файле не верны");
            }
        } catch (IOException e) {
            logger.error("Ошибка IO");
            //e.printStackTrace();
        }

        return words;
    }


    /**
     * Доступность файла для чтения
     * @return true - если можно читать, false - в противном случае
     * */
    @Override
    public boolean ready() {
        try {
            return reader.ready();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

}

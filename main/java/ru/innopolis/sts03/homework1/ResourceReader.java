package ru.innopolis.sts03.homework1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс - Ресурс
 */
public abstract class ResourceReader<T> {
    private static Logger logger = LoggerFactory.getLogger(ResourceFileReader.class);

    protected T resource;   //Ресурс

    /**
     * Конструктор
     *

     * @param T resource - Ресурс из которого читаем данные. В качестве ресурса используется Файл, поток, URL
     * */
    public ResourceReader(T resource) {
        this.resource = resource;
    };


    /**
     * Метод читает данные из нужного ресурса
     * */
    public abstract List<String> readData();

    /**
     * Готов ли для чтения ResourceReader
     * */
    public abstract boolean ready();

    /**
     * Метод получения данных из строки
     * Если в строке есть некорректные символы (латиские буквы), возвращаем null
     *
     * @param data - строка с данными
     * @return возвращает List<String> каждый элемент которого - кириллическое слово, если на входе есть латинские символы, возвращает null
     * */
    protected List<String> parseData(String data) {
        Parser parser = new Parser(data, new ValidatorImpl());
        return parser.parse();
    }


}
